import 'package:flutter/material.dart';
import './Pages/home.dart';
import './Pages/products_admin.dart';
// import 'package:flutter/rendering.dart';

main() {
  // debugPaintSizeEnabled=true;
  // debugPaintBaselinesEnabled=true;
  // debugPaintPointersEnabled=true;

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
          brightness: Brightness.dark,
          primarySwatch: Colors.deepOrange,
          accentColor: Colors.purple),
      // home: Home(),
      routes: {
        '/':  (context) => Home(),
        'product_admin':(context)=> ProductsAdminPage()
      },
    );
  }
}
