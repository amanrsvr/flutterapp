import 'package:flutter/material.dart';
import './Pages/product_details.dart';

class Products extends StatelessWidget {
  final List<String> products;
  final Function deleteProduct;

  Products(this.products, {this.deleteProduct});

  Widget _buildProduct(BuildContext context, int pos) {
    return Card(
      child: new InkWell(
        onTap: () => {
              Navigator.push<bool>(
                context,
                MaterialPageRoute(
                  builder: (context) =>
                      ProductDetatils(products[pos], 'assets/dog.jpg'),
                ),
              ).then((bool delete) {
                print(delete);
                if (delete) {
                  deleteProduct(pos);
                }
              })
            },
        child: Column(
          children: <Widget>[
            Image.asset('assets/dog.jpg'),
            Text(products[pos]),
            // ButtonBar(
            //   alignment: MainAxisAlignment.center,
            //   children: <Widget>[
            //     FlatButton(
            //       child: Text("Details"),
            //       onPressed: () => Navigator.push<bool>(
            //             context,
            //             MaterialPageRoute(
            //               builder: (context) =>
            //                   ProductDetatils(products[pos], 'assets/dog.jpg'),
            //             ),
            //           ).then((bool delete) {
            //             print(delete);
            //             if (delete) {
            //               deleteProduct(pos);
            //             }
            //           }),
            //     )
            //   ],
            // )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    Widget productICard = Center(
      child: Text("No product found, please add some proucts"),
    );

    if (products.length > 0) {
      productICard = ListView.builder(
        itemBuilder: _buildProduct,
        itemCount: products.length,
      );
    }

    return productICard;
  }
}
