import 'package:flutter/material.dart';
import 'dart:async';

class ProductDetatils extends StatelessWidget {
  final String title;
  final String imageUrl;

  ProductDetatils(this.title, this.imageUrl);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () {
          print("on back pressed");
          Navigator.pop(context, false);
          return Future.value(false);
        },
        child: Scaffold(
          appBar: AppBar(
            title: Text('Product Details'),
          ),
          body: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start, //vertically
              crossAxisAlignment: CrossAxisAlignment.center, //horizontally
              children: <Widget>[
                Image.asset(imageUrl),
                Text("on the product details page of  $title"),
                RaisedButton(
                  child: Text("Delete"),
                  onPressed: () => {Navigator.pop(context, true)},
                )
              ],
            ),
          ),
        ));
  }
}
