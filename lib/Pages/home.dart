import 'package:flutter/material.dart';
import '../product_manager.dart';
import '../Pages/products_admin.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('EasyList'),
        ),
        drawer: Drawer(
          child: Column(
            children: <Widget>[
              AppBar(
                automaticallyImplyLeading: false,
                title: Text("Drawer"),
              ),
              ListTile(
                title: Text("Manage Product"),
                onTap: () {
                  Navigator.pushNamed(context, 'product_admin');
                },
              ),
            ],
          ),
        ),
        body: ProductManager());
  }
}
