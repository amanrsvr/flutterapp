import 'package:flutter/material.dart';
import './Products.dart';

class ProductManager extends StatefulWidget {
  String startingProduct;

  ProductManager({this.startingProduct});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ProductManager();
  }
}

class _ProductManager extends State<ProductManager> {
  List<String> products = List();

  @override
  void initState() {
    if (widget.startingProduct != null) products.add(widget.startingProduct);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Container(
        margin: EdgeInsets.all(10.0),
        child: ProducControl(_addProduct),
      ),
      Expanded(child: Products(products, deleteProduct: _deleteProduct,))
    ]);
  }

  void _addProduct() {
    setState(() {
      products.add("product ${products.length + 1}");
    });
  }

  void _deleteProduct(int index) {
    setState(() {
     products.removeAt(index);
    });
  }

}


class ProducControl extends StatelessWidget {
  final Function addProduct;

  ProducControl(this.addProduct);

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      color: Theme.of(context).accentColor,
      child: Text('Add Product'),
      onPressed: () {
        addProduct();
      },
    );
  }
}
